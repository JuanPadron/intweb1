const config = require('../config');
const fs = require('fs');
const {MongoClient, ObjectId} = require('mongodb');
const bcrypt = require('bcrypt');
const { query } = require('express');
const { Console } = require('console');
const expressSession = require('express-session');


/*
For the uri change the format below: 
Change the Username to the username i make for you
Juan: jaun_pad
James: james_bush
The password field and the database are fine as they are just change out the user name or you can connect to mine
i also will have to add yor guys IP address to Atlas so you can connect to it from your homes
mongodb+srv://<username>:uJdpYtAE8A.-YuJ@cluster0.rlsc4.mongodb.net/dataexpress?retryWrites=true&w=majority
*/

const url = "mongodb+srv://carlos_duran1:uJdpYtAE8A.-YuJ@cluster0.rlsc4.mongodb.net/dataexpress?retryWrites=true&w=majority"
const client = new MongoClient(url);
//Database things
const dbName = 'dataexpress';
const db = client.db(dbName);
const col = db.collection('data');



exports.index = (req, res) => {
    res.render('Index', {
        title: 'Home',
        config      //config: config ; both are the same so config is all you need
    });
};

exports.orders = (req, res) => {
    res.render('Orders', {
        title: 'Orders',
        config
    });
};

exports.login = (req, res) => {
    res.render('login', {
        title: 'login',
        config
    })
}

exports.create = (req, res) => {
    res.render('create', {
        title: 'create',
        config
    })
}

exports.features = (req, res) => {
    res.render('Features', {
        title: 'Features',
        config
    });
};

exports.dinosaur = (req, res) => {
    let name = req.params.name;
    res.render('Dinosaurs', {
        title: `${name}'s`,
        config
    })
}

const SaltAndHash = pass => {
    let salt = bcrypt.genSaltSync(10);
    let hash = bcrypt.hashSync(pass, salt);
    return hash;
}

exports.log = async (req, res) => {
    let SHpass = SaltAndHash(req.body.password);
    let account = {
        email: req.body.email
    }

    await client.connect();
    let data = {
        pass: SHpass,
        email: req.body.email,
        quest1: req.body.q1,
        quest2: req.body.q2,
        quest3: req.body.q3,
    }

    const insertRes = await col.insertOne(data);
    client.close();

    res.render('log', {
        title: 'Login Saved',
        account

    })
    
}


exports.edit = async (req, res) => {
    account = {
        email: req.session.user.email
    }
    console.log(req.session.user)

    await client.connect();
    const filterDocs = await col.find(account).toArray();
    client.close();
    res.render('edit', {
        title: 'Edit Question',
        quest: filterDocs[0],
        config
    })
}

exports.editInfo = async (req, res) => {
    await client.connect();
    const update = await col.updateOne(
        { email: req.session.user.email },
        { $set: {
            quest1: req.body.q1,
            quest1: req.body.q2,
            quest1: req.body.q3
        }}
    )
    client.close();
}

exports.submitted = (req, res) => {
    let person = {
        Nasasaures: req.body.types,
        Features: req.body.kinds,
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
        address: req.body.address,
        city: req.body.city,
        state: req.body.state,
        zipcode: req.body.zipcode,
        phoneN: req.body.phonenumber
    };
    let personData = `
        <p>
        Nasasaures: ${person.Nasasaures}<br />
        Features: ${person.Features}<br />
        First Name: ${person.fname}<br />
        Last Name: ${person.lname}<br />
        Email: ${person.email}<br />
        Address: ${person.address}<br />
        City: ${person.city}<br />
        State: ${person.state}<br />
        Zip Code: ${person.zipcode}<br />
        Phone Number: ${person.phoneN}<br />
    ----------------------------------<br />
        </p>
    `;
    
    fs.appendFile('public/submissions.html', personData, err => {
        if(err) throw err;
        console.log('Data has been svaed!')
    });

    res.render('submitted', {
        title: 'Form Accepted',
        person
    })  
};