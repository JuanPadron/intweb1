const express = require('express');
const {MongoClient, ObjectId} = require('mongodb');
const bcrypt = require('bcrypt');
const pug = require('pug');
const path = require('path');
const routes = require('./routes/routes');
const expressSession = require("express-session");

const app = express();

const url = "mongodb+srv://james_bush:uJdpYtAE8A.-YuJ@cluster0.rlsc4.mongodb.net/dataexpress?retryWrites=true&w=majority"
const client = new MongoClient(url);
//Database things
const dbName = 'dataexpress';
const db = client.db(dbName);
const col = db.collection('data');

app.set('view engine', 'pug');
app.set('views', __dirname + '/views');

app.use(express.static(path.join(__dirname, '/public')));
app.use(expressSession({
    secret: "wh4t3v3r",
    saveUninitialized: true,
    resave: true
}));

const checkAuth = (req, res, next) => {
    if(req.session.user && req.session.user.isAuthenticated) {
        next();
    }else{
        res.redirect('/login');
    }
}

const urlendcodedParser = express.urlencoded({
    extended: false
});

app.get('/', routes.index);
app.get('/Home', routes.index);
app.get('/Orders', routes.orders);
app.get('/Dinosaurs/:name', routes.dinosaur);
app.get('/Features', routes.features);
app.get('/login', routes.login);
app.post('/login', urlendcodedParser, async (req, res) => {
    console.log(req.body.email);
    let authSearch = {
        email: req.body.email
    };
    
    await client.connect();

    let auth = await col.find(authSearch).toArray();
    console.log(auth);
    client.close();

    if (auth[0].email == req.body.email && (bcrypt.compareSync(req.body.password, auth[0].pass)) == true){
        console.log("true")
        req.session.user = {
            isAuthenticated: true,
            email: auth[0].email
        };
        res.redirect('/edit');
    } else {
        console.log("false")
        res.redirect('/login');
    }
});
app.get('/create', routes.create);
app.post('/log', urlendcodedParser, routes.log)
app.post('/submitted', urlendcodedParser, routes.submitted);
app.get("/edit", checkAuth, routes.edit);
app.post("/edit", checkAuth, urlendcodedParser, routes.editInfo);
app.get('/logout', (req, res) => {
    req.session.destroy(err => {
        if(err){
            console.log(err);
        }else{
            res.redirect('/');
        }
    });
});



app.listen(3000);
