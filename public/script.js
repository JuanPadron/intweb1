const fname = document.getElementById('fname');
const lname = document.getElementById('lname');
const email = document.getElementById('email');
const adres = document.getElementById('address');
const city = document.getElementById('city');
const state = document.getElementById('state');
const zCode = document.getElementById('zipcode');
const phNum = document.getElementById('phonenumber');
const myErrors = document.getElementById('myErrors');
const passw = document.getElementById('pass')

const name_pattern = /.*[a-z].*[a-z].*/i;
const email_pattern = /.+@.{2,}\..{2,}/;
const address_pattern = /^[a-zA-Z0-9\s,.'-]{3,}$/;
const zip_pattern = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
const phone_pattern = /^(1?\([0-9]{3}\)( |)|(1-|1)?[0-9]{3}-?)[0-9]{3}-?[0-9]{4}$/m;

let error_msg = '';
let no_errors = true;

const validate = () => {
    error_msg = '';
    no_errors = true;   
    if(!phone_pattern.test(phNum.value)){
        error_msg = `Phone Number can be any format in Example:<br /> 1(801)555-1212 <br />
                    1(801) 555-1212 <br /> (801)555-1212 <br /> (801) 555-1212 <br />
                    801-555-1212 <br /> 8015551212 <br />`;
        no_errors = false;
    }
    if(!zip_pattern.test(zCode.value)){
        error_msg = `Zip code must be 5 digits or 5 digits plus a '-' followed by 4 digits (i.e. 84111 or 84111-0294) <br />`;
        no_errors = false;
    }
    if(!name_pattern.test(city.value)){
        error_msg = `City must contain at least two letters <br />`;
        no_errors = false;
    }
    if(!address_pattern.test(adres.value)){
        error_msg = `Address must have at least one character, followed by a space 
                        and at least one or more other characters<br />`;
        no_errors = false;
    }
    if(!email_pattern.test(email.value)){
        error_msg = `Email must have at least one character, followed 
                    by an '@',and at least two characters, followed by 
                    a dot, and at least two more characters<br />`;
        no_errors = false;
    }
    if(!name_pattern.test(lname.value)){
        error_msg = 'Last name must contain at least two letters <br />';
        no_errors = false;
    }
    if(!name_pattern.test(fname.value)){
        error_msg = 'First Name must contain at least two letters <br />';
        no_errors = false;
    }
    myErrors.innerHTML = error_msg;
};

const emVal = () =>{
    error_msg = '';
    no_errors = true;
    if(!email_pattern.test(email.value)){
        error_msg = `Email must have at least one character, followed 
                    by an '@',and at least two characters, followed by 
                    a dot, and at least two more characters<br />`;
        no_errors = false;
    };
    myErrors.innerHTML = error_msg;
}

const emSubmit = () => {
    emVal();
    return no_errors;
}

const mySubmit = () => {
    console.log('1');
    validate();
    console.log('2');
    // return false;
    return no_errors;
}